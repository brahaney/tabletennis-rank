var express = require("express"),
    _und = require("underscore"),
    jf = require("jsonfile");

// DATA MODELS
var Model = {
    Game: function Game(game) {
        this.id = game.id || "new";
        this.players = game.players || [game.winner, game.loser];
        this.playerRanks = game.playerRanks || [game.winnerRank, game.loserRank];
        this.exchanged = game.exchanged;
        this.score = game.score || [game.high, game.low];
        this.timestamp = game.timestamp || now();
        this.spread = game.spread || game.high - game.low;
    },
    Player: function Player(player) {
        this.id = player.id || "new";
        this.name = player.name;
        this.addedTimestamp = player.addedTimestamp || now();
        this.games = player.games || 0;
        this.score = player.score || 100;
        this.currentStreak = player.currentStreak || 0;
        this.longestStreak = player.longestStreak || 0;
        this.wins = player.wins || 0;
        this.losses = player.losses || 0;
        this.scoreChange = player.scoreChange || 0;
    }

};

//FILE MANAGEMENT
function LocalStorage() {
    var file = "/tmp/sdpingpong.json",
        data = {},
        self = this;
    //READ file
    this.Read = function Read() {
        jf.readFile(file, function (err, obj) {
            if (!err) {
                data = obj;
            }
        });
    };
    //WRITE file
    this.Write = function Write() {
        jf.writeFile(file, data, function (err) {
            if (!err) {

            }
        });
    };
    this.maxID = function maxID(collection) {
        result = _und.max(_und.pluck(data[collection], "id"));
    };
    //GET - returns cloned dataset requested
    this.list = function list(options) {
        var result;
        if (typeof options.limit === "number") {
            result = JSON.parse(JSON.stringify(_und.last(data[options.collection], options.limit)));
        } else {
            result = JSON.parse(JSON.stringify(data[options.collection]));
        }
        if (options.collection === "games") { //Map games as Game objects using object models
            result = _und.map(result, function (element) {
                return new Model.Game(element);
            });
        } else if (options.collection === "players") { //Map players as Player objects using object models
            result = _und.map(result, function (element) {
                return new Model.Player(element);
            });
        }
        return result;
    };
    this.where = function where(options) {
        var result;
        result = JSON.parse(JSON.stringify(_und.where(data[options.collection], options.query)));
    };
    this.update = function update(options) {
        var result;
        result = _und.findWhere(data[options.collection], {id: options.id});
        result = _und.extend(result, options.data);
        return result;
    };
    this.update = function update(options) {
        var result;
        result = _und.findWhere(data[options.collection], options.query);
        result = _und.extend(result, options.data);
        return result;
    };
    this.add = function update(options) {
        options.data.id = this.maxID(options.collection) + 1;
        data[options.collection].push(options.data);
        self.Write();
        return {
            id: options.data.id
        };
    };
    this.remove = function remove(options) { //Remove 
        var result, i;
        for (i = 0; i < data[options.collection]; i++) {
            if (data[options.collection][i].id === options.id) {
                data[options.collection].splice(i, 1);
            }
        }
    }
}
var localStorage = new LocalStorage();

// OPERATIONS
var latestGameID = -1;

function NewGame(request, response) {
    var game = new Game(request.body);
    var id = localStorage.add({
        collection: "games",
        data: game
    });
    latestGameID = id; //For use with UNDO
    response.send({
        id: id
    });
}

function UndoLastGame(request, response) {
    if (latestGameID !== -1) {
        localStorage.remove({
            collection: "games",
            id: latestGameID
        });
        latestGameID = -1;
        response.send({
            status: "success"
        });
    } else {
        response.send({
            status: "error",
            error: "Nothing to undo"
        });
    }

    function NewPlayer(request, response) {
        var player = new Player(request.body);
        var id = localStorage.add({
            collection: "players",
            data: player
        });
        response.send({
            id: id;
        });
    }

    function GetPlayerDetails(request, response) {
        
    }

    function GetPlayers(request, response) {
        var players = localStorage.list({collection: "players"});
        response.send({players: players});
    }

    function GetGames(request, response) {
        var games = localStorage.list({collection: "games"});
        response.send({games: games});
    }

    function UpdatePlayer(request, response) {
        var updatedPlayer = localStorage.update({
            collection: "players",
            id: request.params.playerID,
            data: request.body
        });
        response.send({player: updatedPlayer});
    }

    function DeletePlayer(request, response) {
        var localStorage.remove({
            collection: "players",
            id: request.params.playerID
        });
        response.send({status: "success"});
    }

    function UpdateScores(winner, loser, exchange) {
        
    }

    // ROUTES
    var webserver = express();
    //   PLAYERS
    webserver.get("/api/players", GetPlayers);
    webserver.post("/api/players", NewPlayer);
    //   PLAYERS/:PLAYERID
    webserver.get("/api/players/:playerID", GetPlayerDetails);
    webserver.put("/api/players/:playerID", UpdatePlayer);
    webserver.del("/api/players/:playerID", DeletePlayer);
    //   GAMES
    webserver.get("/api/games", GetGames);
    webserver.post("/api/games", NewGame);
    //   UNDO
    webserver.post("/api/undo", UndoLastGame);

    // INIT WEBSERVER

    webserver.listen(1337);